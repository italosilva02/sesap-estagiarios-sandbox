const router = require('express').Router();
const usuariosRoutes = require('./usuarios');
const relatoriosIndexRoutes = require('./relatorios');

router.get('/', (req, res) => {
    res.render('index');
});

router.get('/pagina', (req, res) => {
    res.send('pagina');
});

router.post('/mensagem', (req, res) => {
    res.send('mensagem postada');
});

router.use('/usuarios', usuariosRoutes);
router.use('/relatorios', relatoriosIndexRoutes);

module.exports = router;

