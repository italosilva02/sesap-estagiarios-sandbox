# Sepsap-estagiários

Sistema de gestão de estagiários Open Source sob licença MIT desenvolvido para Secretaria de Saúde Pública do Estado do Rio Grande do Norte (SESAP-RN) com o objetivo de contribuir para a gestão dos estagiários da área de saúde, principalmente frente a crescente demanda devido ao surto do covid-19.

# Como começar

Para executar o projeto, execute os seguintes comandos:

```console
$ npm install
$ npm start
```
Após estes comandos, o sistema irá subir na porta 3000 e poderá ser acessado pelo browser através do seguinte endereço:

[http://localhost:3000](http://localhost:3000)

## Usando Docker

Se preferir, você pode ininicar usando o Docker. Para isso, crie uma imagem local e em seguida execute o comando docker run.

```console
$ docker build -t sesap-covid19 .
$ docker run -p 3000:3000 -d sesap-covid19
```

Se quiser, você pode executar uma imagem pronta de nosso repositório.

Para executar a última versão de desenvolvimento:

```console
$ docker run -p 3000:3000 -d registry.gitlab.com/covid-192/sesap/sesap-estagiarios-sandbox:dev
```

ou execute o seguinte comando para executar a última versão de produção (branch master)

```console
$ docker run -p 3000:3000 -d registry.gitlab.com/covid-192/sesap/sesap-estagiarios-sandbox:latest
```

## Contribibuindo

É novo por aqui? Este projeto é desenvolvido completamente através de trabalho voluntário e gerenciado por docentes do IMD/UFRN. Gostou da causa? Clique [aqui](CONTRIBUTING.md) e saiba mais de como contribuir.
